import React from 'react';

const Country = ({ name, clicked}) => {
    return (
        <div className="country-item" onClick={clicked}>
            <a onClick={clicked} href="#" className="country-link">{name}</a>
        </div>
    );
};

export default Country;