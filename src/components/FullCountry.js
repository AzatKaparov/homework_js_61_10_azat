import React, {useEffect, useState} from 'react';
import axios from "axios";

const FullCountry = ({ id }) => {
    const [country, setCountry] = useState(null);

    useEffect(() => {

        const fetchData = async () => {
            if (id !== null) {
                const countryResponse = await axios.get(`rest/v2/alpha/${id}`);
                setCountry(countryResponse.data);
            }
        }
        fetchData().catch(console.error)
    }, [id]);

    if (country === null) {
        return <h1>You need to chose country first</h1>
    }

    return country && (
        <div className="country-info">
            <div className="flag-block">
                <img className="country-flag" src={country.flag} alt={country.name}/>
            </div>
            <p className="country-info-item">Capital: <span>{country.capital}</span></p>
            <p className="country-info-item">Population: <span>{country.population}</span></p>
            <p className="country-info-item">Region: <span>{country.region}</span></p>
            <h2>Borders with:</h2>
            <ul>
                {country.borders.map(item => {
                    return (
                        <li>{item}</li>
                    );
                })}
            </ul>
        </div>
    );
};

export default FullCountry;