import Countries from "./containers/Countries";
import './App.css';

function App() {
  return (
    <div className="container">
      <Countries/>
    </div>
  );
}

export default App;
