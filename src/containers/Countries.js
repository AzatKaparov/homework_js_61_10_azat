import React, {useState, useEffect} from 'react';
import './Countries.css';
import axios from "axios";
import Country from "../components/Country";
import FullCountry from "../components/FullCountry";

const Countries = () => {
    const [countries, setCountries] = useState([]);
    const [selectedCountry, setSelectedCountry] = useState(null);


    useEffect(() => {
        const fetchData = async () => {
            const countriesResponse = await axios.get("rest/v2/all?fields=name;alpha3Code");
            setCountries(countriesResponse.data);
        };

        fetchData().catch(console.error);
    }, [])

    return (
        <div className="countries">
            <h1 className="main-title">Countries info</h1>
            <div className="countries-list">
                {countries.map(country => {
                    return (
                        <Country
                            name={country.name}
                            key={country.alpha3Code}
                            id={selectedCountry}
                            clicked={e => {
                                e.preventDefault();
                                setSelectedCountry(country.alpha3Code);
                            }}
                        />
                    )
                })}
            </div>
            <div className="county-info-block">
                <FullCountry id={selectedCountry}/>
            </div>
        </div>
    );
};

export default Countries;